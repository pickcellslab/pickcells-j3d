package org.pickcellslab.pickcells.j3d.scenegraph;

public interface Scene3DModelListener {

  public void modelChanged(Scene3DModelEvent evt);

}
