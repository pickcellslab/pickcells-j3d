package org.pickcellslab.pickcells.j3d.scenegraph;

import javax.media.j3d.Appearance;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.TransparencyAttributes;

public class EdgeAppearance extends Appearance {


  public EdgeAppearance() {
    createValue();
  }

  public void createValue() {

    TransparencyAttributes att = new TransparencyAttributes(TransparencyAttributes.NICEST, 0.8f);
    // att.setSrcBlendFunction(TransparencyAttributes.NICEST);

    this.setTransparencyAttributes(att);
    att.setCapability(TransparencyAttributes.ALLOW_VALUE_READ);
    att.setCapability(TransparencyAttributes.ALLOW_VALUE_WRITE);
    att.setCapabilityIsFrequent(TransparencyAttributes.ALLOW_VALUE_READ);
    att.setCapabilityIsFrequent(TransparencyAttributes.ALLOW_VALUE_WRITE);
    

    PolygonAttributes facettes = new PolygonAttributes(PolygonAttributes.POLYGON_LINE,
        PolygonAttributes.CULL_FRONT, -0.001f, true);
    facettes.setCapability(PolygonAttributes.ALLOW_MODE_READ);
    facettes.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);

    this.setPolygonAttributes(facettes);

    RenderingAttributes rendering = new RenderingAttributes();
    // rendering.setDepthBufferEnable(true);
    rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_READ);
    rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_WRITE);
    rendering.setVisible(true);
    this.setRenderingAttributes(rendering);

    this.setCapability(Appearance.ALLOW_MATERIAL_READ);
    this.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
    this.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_READ);
    this.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
    this.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
    this.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);
    
    
  }

}
