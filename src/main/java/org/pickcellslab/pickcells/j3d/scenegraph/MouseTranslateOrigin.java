package org.pickcellslab.pickcells.j3d.scenegraph;

import javax.media.j3d.TransformGroup;

import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;

public class MouseTranslateOrigin extends MouseTranslate {
	
	public MouseTranslateOrigin(TransformGroup group, MouseRotate rotator){
		super(group);
	}
}
