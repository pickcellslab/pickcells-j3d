package org.pickcellslab.pickcells.j3d.scenegraph;

import java.awt.Color;
import java.util.ArrayList;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.PointArray;
import javax.media.j3d.Shape3D;
import javax.vecmath.Color4f;
import javax.vecmath.Point3d;

public class ColoredPointGeometry extends Shape3D {

  public ColoredPointGeometry(ArrayList<Point3d> coordinates, Color color, double scale) {

    GeometryArray array = new PointArray(coordinates.size(), GeometryArray.COORDINATES
        | GeometryArray.COLOR_4);

    int index = 0;

    for (Point3d p : coordinates) {

      p.scale(scale);

      array.setCoordinate(index, p);
      array.setColor(index, new Color4f(color));
      index++;

    }

    this.setGeometry(array);

  }

}
