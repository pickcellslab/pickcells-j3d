package org.pickcellslab.pickcells.j3d.scenegraph;

import java.awt.Color;

import javax.media.j3d.Appearance;
import javax.media.j3d.Material;
import javax.media.j3d.RenderingAttributes;
import javax.vecmath.Color3f;

public class VectorAppearance extends Appearance {

  private Color3f color;

  public VectorAppearance() {
    color = new Color3f(Color.WHITE);
    createValue();
  }

  public VectorAppearance(Color3f color) {
    this.color = color;
    createValue();
  }

  public void setColor(Color3f color) {
    this.color = color;
    createValue();
  }

  private void createValue() {

    Material colorMat = new Material();

    colorMat.setCapability(Material.ALLOW_COMPONENT_READ);
    colorMat.setCapability(Material.ALLOW_COMPONENT_WRITE);

    colorMat.setShininess(128);
    // colorMat.setAmbientColor(new Color3f(color.x, color.y, color.z));
    colorMat.setSpecularColor(new Color3f(Color.WHITE));// 0.3f, 0.3f, 0.05f));
    colorMat.setDiffuseColor(color);

    this.setMaterial(colorMat);

    RenderingAttributes rendering = new RenderingAttributes();
    // rendering.setDepthBufferEnable(true);
    
    rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_READ);
    rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_WRITE);
    //rendering.setVisible(false);
    this.setRenderingAttributes(rendering);

  }

}