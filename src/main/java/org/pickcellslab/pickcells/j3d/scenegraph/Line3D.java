package org.pickcellslab.pickcells.j3d.scenegraph;

import java.awt.Color;

import javax.media.j3d.LineArray;
import javax.media.j3d.Shape3D;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;

public class Line3D extends Shape3D {

  public Line3D(Point3d origin, Point3d destination, Color color) {

    Point3d[] coords = {origin, destination};
    Color3f[] colors = {new Color3f(color), new Color3f(color)};

    LineArray lineArray = new LineArray(2, LineArray.COORDINATES | LineArray.COLOR_3);
    lineArray.setCoordinates(0, coords);
    lineArray.setColors(0, colors);

    this.setGeometry(lineArray);
  }
}
