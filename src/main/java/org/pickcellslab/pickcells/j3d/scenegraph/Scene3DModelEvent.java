package org.pickcellslab.pickcells.j3d.scenegraph;

import java.util.EventObject;

public class Scene3DModelEvent extends EventObject {

  private static final long serialVersionUID = 1L;
  public String event = "";

  public Scene3DModelEvent(Object source, String event) {
    super(source);
    this.event = event;
  }

}
