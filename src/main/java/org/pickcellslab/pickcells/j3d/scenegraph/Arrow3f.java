package org.pickcellslab.pickcells.j3d.scenegraph;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Point3f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.Cone;
import com.sun.j3d.utils.geometry.Cylinder;

public class Arrow3f extends BranchGroup {

	private final Cylinder cylinder;
	private final Cone cone;


	// Create the object Arrow3D
	public Arrow3f(double d, double e, Point3f origin, Point3f head) {


		// Arrowhead
		cone = new Cone((float) d, (float) e);
		//cone.setAppearance(app);

		// Arrowbody
		float length = (float) head.distance(origin);

		if (!Float.isNaN(length)) {

			cylinder = new Cylinder((float) d / 5, length);
			// cylinder.setAppearance(app);

			// Position of arrowhead with respect to body
			TransformGroup tg = new TransformGroup();

			Transform3D headtranslation = new Transform3D();
			headtranslation.setTranslation(new Vector3f(0.0f, length / 2f, 0.0f));
			tg.setTransform(headtranslation);
			tg.addChild(cone);

			


			// Position of the arrow in the scene
			// Move to origin
			TransformGroup arrowTG = new TransformGroup();
			arrowTG.addChild(tg);
			arrowTG.addChild(cylinder);
			
			
			TransformGroup movetoOriginTG = new TransformGroup();
			Transform3D transToOrigin = new Transform3D();
			transToOrigin.setTranslation(new Vector3f(0, (float) (length / 2), 0));
			movetoOriginTG.setTransform(transToOrigin);
			movetoOriginTG.addChild(arrowTG);

			// Position the arrow to the final position
			TransformGroup finalTG = new TransformGroup();
			Transform3D lastTrans = new Transform3D();
			// Rotate the arrow
			// Calculate angle of rotation
			Vector3f initialVector = new Vector3f(0, 1, 0);

			Vector3f finalVector = new Vector3f((float) (head.x - origin.x), (float) (head.y - origin.y),
					(float) (head.z - origin.z));

			float angle = initialVector.angle(finalVector);

			float opposite = initialVector.angle(new Vector3f(0, -1, 0));

			if (!(initialVector.equals(finalVector) || angle == 0)) {

				// Calculate cross product
				Vector3f cross = new Vector3f();
				cross.cross(initialVector, finalVector);
				cross.normalize();

				// Make quaternion
				// q = <cos(alpha/2), sin(alpha/2)*ux, sin(alpha/2)*uy, sin(alpha/2)*uz>
				Quat4f q = null;
				if (angle != opposite) {
					q = new Quat4f((float) (Math.sin(angle / 2) * cross.x), (float) Math.sin(angle / 2)
							* cross.y, (float) Math.sin(angle / 2) * cross.z, (float) Math.cos(angle / 2));
				}
				else {
					q = new Quat4f(0, 0, (float) Math.PI, 0);
				}

				// Rotate
				lastTrans.setRotation(q);

			}

			// Translate to final position
			lastTrans.setTranslation(new Vector3f(origin));
			finalTG.setTransform(lastTrans);

			finalTG.addChild(movetoOriginTG);

			// arrowTG.addChild(this);
			
			
			
			this.addChild(finalTG);

		}
		else
			cylinder = null;

	}


	public void setAppearance(Appearance app){
		if(null!=cylinder)
			cylinder.setAppearance(app);
		cone.setAppearance(app);
	}


}
