package org.pickcellslab.pickcells.j3d.view;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.media.j3d.Appearance;
import javax.media.j3d.ColoringAttributes;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.RenderingAttributes;
import javax.media.j3d.Texture;
import javax.media.j3d.Texture2D;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.vecmath.Color3f;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.actions.SimpleStatsAction;
import org.pickcellslab.foundationj.services.theme.UITheme;

import com.sun.j3d.utils.image.TextureLoader;

public class AppearanceManager {


	private final Color[] palette;
	
	public enum Scenary{
		Location, Vector, Bounds
	}


	private static final String[] files = {"/textures/rainbow.png", "/textures/fire.png", "/textures/blue.png"};

	private final Map<String,Appearance> apps = new HashMap<>();
	private final Map<String, List<HeatMappable>> heatMapObjects = new HashMap<>();
	private final Map<String, Dimension> dims = new HashMap<>();

	private final Texture[] textures;


	public AppearanceManager(UITheme theme) {

		this.palette = theme.defaultPalette();
		
		//Load textures

		textures = new Texture[files.length];

		for(int i = 0; i<files.length; i++){
			TextureLoader loader = new TextureLoader(getClass().getResource(files[i]), null);
			ImageComponent2D image = loader.getImage();
			if(image == null) {
				System.out.println("load failed for texture : "+files[i]);
			}
			textures[i] = new Texture2D(Texture.BASE_LEVEL, Texture.RGBA, image.getWidth(), image.getHeight());
			textures[i].setImage(0, image);
		}



	}




	public Dimension getCurrentHeatMapDimension(String series) {
		return dims.get(series);
	}



	public int getCurrentTextureIndex(String series){
		Texture t = apps.get(series).getTexture();
		if(t == null)
			return -1;
		for(int i = 0; i<files.length; i++)
			if(t == textures[i])
				return i;
		return -2;
	}





	public List<Dimension> availableHeatMapDimensions(String series, DataAccess access){

		List<HeatMappable> list = heatMapObjects.get(series);
		if(list == null || list.isEmpty())
			return Collections.emptyList();

		return list.get(0).availableDimensions(access);

	}



	public synchronized void addHeatMappable(String s, HeatMappable heatMappable) {
		List<HeatMappable> list = heatMapObjects.get(s);
		if(list == null){
			list = new ArrayList<>();
			heatMapObjects.put(s, list);
			System.out.println("HeatMappable added for "+s);
		}
		list.add(heatMappable);
	}


	/**
	 * @param s
	 * @param lut null removes any texture the given series may possess
	 */
	public void setHeatMapLut(String s, Texture lut){
		Appearance app = apps.get(s);
		if(app!=null)
			app.setTexture(lut);
		else
			System.out.println("Appearance for "+s+" is null");
	}



	public <N extends Number> void setHeatMapDimension(String s, Dimension<DataItem,N> dim, DataAccess access){

		List<HeatMappable> list = heatMapObjects.get(s);
		if(list == null || list.isEmpty()){
			System.out.println("Series "+s+" is either empty or does not exist");
			return;
		}



		try{

			//Check if the dim has changed
			if(!dim.equals(dims.get(s))){



				//System.out.println("Setting HeatMap Dimension for "+s+" -> "+dim);	


				// Organise the list into a map with theit ids as keys
				final Map<Integer, HeatMappable> map = new HashMap<>();
				list.forEach(hm ->map.put(hm.getId(),hm));

				final DataPointer v = list.get(0).getVignette(); //TODO datapointer instead
				final AKey<N> k = (AKey<N>) AKey.get("HeatMap", AKey.noPrimitiveClass(dim.getReturnType()));

				//System.out.println("Obtained Pointer "+v);	

				// Check min and max for this dim in the database for scaling
				final SummaryStatistics stats = v.readData(access, new SimpleStatsAction<>(dim), -1);

				//System.out.println("Obtained Pointer "+stats.getMin()+"; "+stats.getMax());	
				
				//System.out.println("Map Size "+map.size());	
				

				// get the values for our dataset
				v.readData(access,new ColorAction<>(map, dim, stats.getMin(), stats.getMax()), -1);

				dims.put(s, dim);

			}

		}catch(Exception e ){
			e.printStackTrace();
		}

	}


	public Texture[] getHeatMapLuts(){
		return textures;
	}


	public Icon[] getHeatMapIcons() {
		Icon[] icons = new Icon[files.length];
		for(int i = 0; i<files.length; i++)
			icons[i] = UITheme.resize(new ImageIcon(getClass().getResource(files[i])), 100, 32);
		return icons;
	}








	public synchronized Appearance getOrCreateAppearance(String s, Scenary sc) {
		Appearance app = apps.get(s);

		if(null == app){
			synchronized(this){
				app = getNext(sc);
				apps.put(s, app);
			}
		}
		return app;
	}





	public Appearance getAppearance(String s){
		return apps.get(s);
	}




	public List<String> registeredSeries(){
		return new ArrayList<>(apps.keySet());
	}






	private synchronized Appearance getNext(Scenary sc) {
		//Get the next color
		Color c = getNextColor();
		Appearance app = createAppearance(c);
		return app;
	}


	private synchronized Color getNextColor(){
		int n =  apps.size() % palette.length;
		return palette[n];
	}




	public Appearance createAppearance(Color c) {

		Appearance app = new Appearance();

		ColoringAttributes cAtts = new ColoringAttributes(); //Check if this works
		cAtts.setColor(new Color3f(c));		
		app.setColoringAttributes(cAtts);

		TransparencyAttributes att = new TransparencyAttributes(TransparencyAttributes.NICEST, 0.8f);
		// att.setSrcBlendFunction(TransparencyAttributes.NICEST);

		app.setTransparencyAttributes(att);
		att.setCapability(TransparencyAttributes.ALLOW_VALUE_READ);
		att.setCapability(TransparencyAttributes.ALLOW_VALUE_WRITE);
		att.setCapabilityIsFrequent(TransparencyAttributes.ALLOW_VALUE_READ);
		att.setCapabilityIsFrequent(TransparencyAttributes.ALLOW_VALUE_WRITE);


		PolygonAttributes facettes = new PolygonAttributes(PolygonAttributes.POLYGON_LINE,
				PolygonAttributes.CULL_BACK,  0, false);
		facettes.setCapability(PolygonAttributes.ALLOW_MODE_READ);
		facettes.setCapability(PolygonAttributes.ALLOW_MODE_WRITE);

		app.setPolygonAttributes(facettes);

		RenderingAttributes rendering = new RenderingAttributes();
		rendering.setDepthBufferEnable(true);
		rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_READ);
		rendering.setCapability(RenderingAttributes.ALLOW_VISIBLE_WRITE);
		rendering.setVisible(true);
		app.setRenderingAttributes(rendering);

		app.setCapability(Appearance.ALLOW_MATERIAL_READ);
		app.setCapability(Appearance.ALLOW_MATERIAL_WRITE);
		app.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_READ);
		app.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
		app.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_READ);
		app.setCapability(Appearance.ALLOW_TRANSPARENCY_ATTRIBUTES_WRITE);


		//TEST
		//app.setTexture(textures[0]);
		app.setCapability(Appearance.ALLOW_TEXGEN_WRITE);
		app.setCapability(Appearance.ALLOW_TEXTURE_READ);
		app.setCapability(Appearance.ALLOW_TEXTURE_WRITE);


		return app;

	}


	private class ColorAction<N extends Number> implements Action<DataItem, Void>{

		final Map<Integer, HeatMappable> map;
		final Dimension<DataItem, N> dim;
		final double min, max;
		public ColorAction(final Map<Integer, HeatMappable> map, final Dimension<DataItem, N> dim, double min, double max) {
			this.map = map;
			this.dim = dim;
			this.min = min;
			this.max = max;
		}
		
		@Override
		public ActionMechanism<DataItem> createCoordinate(Object key) throws DataAccessException {
			return new ColorMecha();
		}

		@Override
		public Set<Object> coordinates() {
			Set<Object> set = new HashSet<>();
			set.add(this);
			return set;
		}

		@Override
		public Void createOutput(Object key) throws IllegalArgumentException {
			return null;
		}

		@Override
		public String description() {
			return "Create a 3D heatmap for "+dim;
		}
		
		
		
		
		
		private class ColorMecha implements ActionMechanism<DataItem>{
					
			@Override
			public void performAction(DataItem i) throws DataAccessException {
				HeatMappable hm = map.get(i.getAttribute(DataItem.idKey).get());
				if(hm!=null){
					N n = dim.apply(i);
					if(n==null){
						hm.setColorDimension(0f, min, max);
					}
					else
						hm.setColorDimension(n.floatValue(), min, max);
				}
			}
			
		}
		
		
		
	}
	
	
	






}
