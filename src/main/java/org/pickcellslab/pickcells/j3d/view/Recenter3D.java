package org.pickcellslab.pickcells.j3d.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.media.j3d.BoundingBox;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.Shape3D;
import javax.vecmath.Point3d;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.picking.PickCanvas;
import com.sun.j3d.utils.picking.PickResult;

public class Recenter3D extends MouseAdapter {

	private static Logger log = LoggerFactory.getLogger(SinglePicker3D.class);

	private final PickCanvas pickCanvas;
	private final OrbitBehavior orbit;
	
	
	public Recenter3D(PickCanvas pickCanvas, OrbitBehavior orbit) {
		this.pickCanvas = pickCanvas;
		this.orbit = orbit;
	}
	
	
	

	public void mouseClicked(MouseEvent e){

		if(e.getButton()==MouseEvent.BUTTON1){

			pickCanvas.setShapeLocation(e);

			PickResult result = pickCanvas.pickClosest();

			if (result == null) {

				log.debug("Nothing picked by the single picker");

			} else {



				Primitive p = (Primitive) result.getNode(PickResult.PRIMITIVE);


				Shape3D s = (Shape3D)result.getNode(PickResult.SHAPE3D);
				log.debug("Checking Shape3D");


				Point3d center = new Point3d();
				
				if(p!=null){

					((BoundingSphere)p.getBounds()).getCenter(center);
					

						/*

						if(current!=null){
							if(Primitive.class.isAssignableFrom(current.getClass())){
								((Primitive)current).setAppearance(previous);
							}
							else if(Shape3D.class.isAssignableFrom(current.getClass())){
								((Shape3D)current).setAppearance(previous);
							}						
						}
						current = p;
						previous = p.getAppearance();
						p.setAppearance(pickedApp);
						 */

						log.info("Recentering on Shape 3D -> "+center.toString());
						orbit.setRotationCenter(new Point3d(center));
					

				} 
				else if (s != null) {


					((BoundingBox)s.getBounds()).getLower(center);
					orbit.setRotationCenter(new Point3d(center));
					
					/*
					if(center!=null){
						log.info("Recentering on Shape 3D -> "+Arrays.toString(center));
						orbit.setRotationCenter(new Point3d(center));
					}else{

						center = (double[]) s.getGeometry().getUserData();
						if(center!=null){
							log.info("Recentering on Geometry -> "+Arrays.toString(center));
							orbit.setRotationCenter(new Point3d(center));
						}

					}
					*/

					log.info("picking processed");

					/*

					if(current!=null){
						if(Primitive.class.isAssignableFrom(current.getClass())){
							((Primitive)current).setAppearance(previous);
						}
						else if(Shape3D.class.isAssignableFrom(current.getClass())){
							((Shape3D)current).setAppearance(previous);
						}						
					}
					current = s;
					previous = s.getAppearance();
					s.setAppearance(pickedApp);
					 */






				} else
					log.info("No Pick Identified");

			}

		}
	}
		
	
}