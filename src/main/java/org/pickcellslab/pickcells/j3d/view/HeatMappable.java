package org.pickcellslab.pickcells.j3d.view;

import java.util.ArrayList;
import java.util.List;

import javax.media.j3d.GeometryArray;
import javax.vecmath.TexCoord2f;

import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;

public class HeatMappable<T extends WritableDataItem> {

	private final GeometryArray geo;
	private final MetaQueryable v;
	private final int dbId;


	public HeatMappable(GeometryArray s, MetaQueryable v, int dbId) {
		geo = s;
		this.v = v;
		this.dbId = dbId;
	}

	
	public DataPointer getVignette(){
		return v;
	}


	public int getId(){
		return dbId;
	}
	
	
	@SuppressWarnings("rawtypes")
	public List<Dimension> availableDimensions(DataAccess access){
			final List<Dimension> dims = new ArrayList<>();
			v.getReadables().filter(mr->mr.dataType() == dType.NUMERIC).forEach(mr->{
				for(int d = 0; d<mr.numDimensions(); d++){
					dims.add(mr.getDimension(d));
				}
			});
			return dims;
	}





	public void setColorDimension(float value, double min, double max) {
		

			float f = (float) ((value-min) / (max-min));
			
			TexCoord2f tc = new TexCoord2f(f, 0f);
			TexCoord2f[] array = new TexCoord2f[geo.getVertexCount()];
			for(int i = 0; i<geo.getVertexCount(); i++)
				array[i] = tc;
			geo.setTextureCoordinates(0,0, array);


		
	}







}
