package org.pickcellslab.pickcells.j3d.view;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.media.j3d.Shape3D;

import org.pickcellslab.foundationj.dbm.access.DataPointer;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPickConsumer;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPicker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.picking.PickCanvas;
import com.sun.j3d.utils.picking.PickResult;

public class SinglePicker3D extends MouseAdapter implements SingleDataPicker{

	private static Logger log = LoggerFactory.getLogger(SinglePicker3D.class);

	private final PickCanvas pickCanvas;
	private final OrbitBehavior orbit;


	public SinglePicker3D(PickCanvas pickCanvas, OrbitBehavior orbit) {
		this.pickCanvas = pickCanvas;
		this.orbit = orbit;
	}



	public void mouseClicked(MouseEvent e){

		if(e.getButton()==MouseEvent.BUTTON3){

			pickCanvas.setShapeLocation(e);

			PickResult result = pickCanvas.pickClosest();
			

			if (result == null) {

				log.debug("Nothing picked by the single picker");

			} else {



				Primitive p = (Primitive) result.getNode(PickResult.PRIMITIVE);
				

				Shape3D s = (Shape3D)result.getNode(PickResult.SHAPE3D);
				log.debug("Checking Shape3D");



				if(p!=null){


					// Primitive means ImageDot

					int dbId = (int) p.getUserData();
					

					fireSingleDataPick(
							(DataPointer) p.getParent().getUserData(),
							dbId,
							e.getPoint());


				} 
				else if (s != null) {


					int dbId = (int) s.getUserData();

					//FIXME handle links here
					fireSingleDataPick(
							(DataPointer) s.getParent().getUserData(),
							dbId,
							e.getPoint());


					log.info("picking processed for "+(DataPointer) s.getParent().getUserData()+" : "+dbId);


				} else
					log.info("No Pick Identified");

			}

		}
	}



	protected final List<SingleDataPickConsumer> singlePickers = new ArrayList<>();


	@Override
	public void addSingleDataPickListener(SingleDataPickConsumer l) {
		if(l!=null)
			singlePickers.add(l);
	}

	@Override
	public void removeSingleDataPicker(SingleDataPickConsumer l) {
		singlePickers.remove(l);
	}

	/**
	 * Notify all listener about a new 
	 * 
	 * @param clazz
	 * @param dbId
	 * @param p
	 */
	protected void fireSingleDataPick(DataPointer dataPointer, int dbId, Point p){
		singlePickers.forEach(l->l.singleDataPicked(dataPointer, dbId));
	}


}
