package org.pickcellslab.pickcells.j3d.view;

import java.util.List;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.Group;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Point3f;
import javax.vecmath.TexCoord2f;
import javax.vecmath.Vector3d;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.j3d.scenegraph.Arrow3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;

public class Factory3D implements Scene3DFactory {

	private static Logger log = LoggerFactory.getLogger(Factory3D.class);

	public Arrow3f createLink(Link l) {

		
		if(l.getAttribute(Keys.sourceAnchor).isPresent() && l.getAttribute(Keys.targetAnchor).isPresent()){
			return createAnchoredLink(l);
		}
		
		

		//origin
		double[] o = l.source().getAttribute(Keys.centroid).orElse(null);
		if(o == null)
			return null;

		double[] h = l.target().getAttribute(Keys.centroid).orElse(null);
		if(h == null)
			return null;


		Point3f ori = new Point3f((float)o[0],(float)o[1],(float)o[2]);
		//head

		Point3f head = new Point3f((float)h[0],(float)h[1],(float)h[2]);
		

		if(!ori.equals(head))
			return new Arrow3f(1f, 1f, ori, head);
		
		return null;
	}




	public  Arrow3f createAnchoredLink(Link l) {
		
		System.out.println("anchored");
		
		//origin
		float[] o = l.getAttribute(Keys.sourceAnchor).get(); 
		Point3f ori = new Point3f(o);
		
		//head
		float[] h = l.getAttribute(Keys.targetAnchor).get(); 
		Point3f head = new Point3f(h);
		
		
		if(!ori.equals(head))
			return new Arrow3f(1f, 1f, ori, head);
		
		return null;

	}






	public Sphere createDot(NodeItem node, Group series) {

		//For each item simply add a sphere
		Transform3D tr = new Transform3D();
		tr.setTranslation(new Vector3d(node.getAttribute(Keys.centroid).get()));

		TransformGroup tg = new TransformGroup();
		tg.setTransform(tr);		

		Sphere sphere = new Sphere();
		sphere.setCapability(Primitive.ENABLE_GEOMETRY_PICKING);
		sphere.setCapability(Primitive.ENABLE_APPEARANCE_MODIFY);
				
		sphere.setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
		
		tg.addChild(sphere);
		
		series.addChild(tg);
		
		return sphere;

	}

	





	public GeometryArray createMesh(List<float[]> mesh) {

		//Create the geometry from the list of Point3f
				GeometryInfo triangleArray = new GeometryInfo(GeometryInfo.TRIANGLE_ARRAY);
				triangleArray.setTextureCoordinateParams(1, 2);
				
				Point3f[] points = new Point3f[mesh.size()];
				TexCoord2f[] tc2D = new TexCoord2f[mesh.size()];
				TexCoord2f color = new TexCoord2f(0f,0f);
				//float inc =  1f/(float)mesh.size();
				//float c =  0f;
				for(int i = 0; i< points.length; i++){
					points[i] = new Point3f(mesh.get(i));	
					tc2D[i] = color;
					//c+=inc;
				}

				
				//triangleArray.setCoordinates(array);//should work!
				triangleArray.setCoordinates(points);
				triangleArray.setTextureCoordinates(0,tc2D);
				
				new NormalGenerator().generateNormals(triangleArray);
				
				GeometryArray geo =  triangleArray.getGeometryArray();	
				
				geo.setCapability(GeometryArray.ALLOW_TEXCOORD_WRITE);
						
				return geo;

	}
	
	
	


	





	
	
	
}
