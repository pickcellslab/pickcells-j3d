package org.pickcellslab.pickcells.j3d.view;

import java.awt.BorderLayout;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.IconID;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.AbstractActivableModule;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Boundary;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.imgdb.view.ImageContentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Module
public class Scene3D extends AbstractActivableModule implements DBViewFactory, MetaModelListener {

	private static Logger log = LoggerFactory.getLogger(Scene3D.class);


	private boolean isActive = false;

	private final ProviderFactoryFactory pff;
	private final NotificationFactory notif;
	private final DataRegistry registry;
	private final UITheme theme;

	private Scene3DFactory factory;

	
	private final Predicate<MetaQueryable> renderable;


	public Scene3D(DataAccess access, ProviderFactoryFactory pff, NotificationFactory notif, UITheme theme){
		this.pff = pff;
		this.notif = notif;
		this.registry = access.dataRegistry();
		this.theme = theme;
		
		renderable = mq ->{
			
			if(mq instanceof MetaClass){
				Class<?> clazz = ((MetaClass) mq).itemClass(registry);
				return ImageLocated.class.isAssignableFrom(clazz) || clazz == Boundary.class;
			}
			else{			
				MetaLink ml = (MetaLink) mq;
				return 
						(ml.hasKey(Keys.sourceAnchor) && ml.hasKey(Keys.targetAnchor)) ||
						ImageLocated.class.isAssignableFrom(ml.source().itemClass(registry)) &&
						ImageLocated.class.isAssignableFrom(ml.target().itemClass(registry));
			}

		};
		
	}




	@Override
	public ViewType getType() {
		return ViewType.Categorical;
	}

	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) throws ViewFactoryException {

		
			try {
				long depth = access.queryFactory().read(Image.class,1).makeList(AKey.get("dimensions", long[].class)).inOneSet().getAll().run().get(0)[Image.z];
				if(depth == 1)
					factory = new Factory2D();
				else
					factory = new Factory3D();
			} catch (DataAccessException e) {
				throw new ViewFactoryException("Could not read images");
			}
		



		View3D view = new View3D(new ImageContentModel(access, pff), factory, notif, theme);
		helper.registerSingleConsumersFor(view.singlePicker());


		// Create Controllers
		
		Consumer<MouseListener> mouseSwitch = (ml) -> view.setPickingControl(ml);
		
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(
						() -> QueryConfigs.newPathOnlyConfig("")
						.setSynchronizeRoots(true)
						.setFixedRootKey(ImageContentModel.fixedRootKey(access))
						.setAllowedQueryables(renderable)
						.build(),
						view.getModel())
				.addToggleControl(
						new Icon[] {theme.icon(IconID.Misc.PICK_ONE, 16), theme.icon(IconID.Misc.PICK_SEVERAL, 16)},
						new String[]{"Enable Single Picking", "Enable Series Picking"},
						view.pickers(), mouseSwitch)
				.addButton(theme.icon(IconID.Misc.PAINT, 16), "Colors...", 
						l->{
							if(view.getModel().getDataSet() == null){
								JOptionPane.showMessageDialog(null, "There is no dataset loaded yet");
								return;
							}
							AppearanceSelector dg = new AppearanceSelector(view.getAppearanceManager(), theme, access);
							dg.pack();
							dg.setLocationRelativeTo(null);
							dg.setModal(true);
							dg.setVisible(true);
							
						})	
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(view.canvas()), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());
	}


	


	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		if(!isActive){
			log.debug("Was not yet activable -> screening changes");
			isActive = !meta.getTargets(MetaQueryable.class).collect(Collectors.toList())
					.stream().anyMatch(ImageContentModel.renderableTypes(registry));		
			this.fireIsNowActive();
		}
	}



	@Override
	public String name() {
		return "3D Scene";
	}

	@Override
	public String description() {
		return "Displays a 3D scene of renderable data";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/mesh.png"));
	}






	@Override
	public boolean isActive() {
		return isActive;
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}














}
