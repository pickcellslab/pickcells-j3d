package org.pickcellslab.pickcells.j3d.view;

import java.util.List;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.Group;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.j3d.scenegraph.Arrow3f;

import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.geometry.Sphere;

public class Factory2D implements Scene3DFactory {



	public Arrow3f createLink(Link l) {

		if(l.getAttribute(Keys.sourceAnchor).isPresent()){
			return createAnchoredLink(l);
		}

		//origin
		double[] o = l.source().getAttribute(Keys.centroid).orElse(null);
		if(o == null)
			return null;

		double[] h = l.target().getAttribute(Keys.centroid).orElse(null);
		if(h == null)
			return null;


		Point3f ori = new Point3f((float)o[0],(float)o[1],0);
		//head

		Point3f head = new Point3f((float)h[0],(float)h[1],0);
		return new Arrow3f(1f, 1f, ori, head);



	}




	public  Arrow3f createAnchoredLink(Link l) {

		//origin
		float[] o = l.getAttribute(Keys.sourceAnchor).get(); 
		Point3f ori = new Point3f(o[0],o[1],0);

		//head
		float[] h = l.getAttribute(Keys.targetAnchor).get(); 
		Point3f head = new Point3f(h[0],h[1],0);


		if(!ori.equals(head))
			return new Arrow3f(1f, 1f, ori, head);

		return null;

	}






	public Sphere createDot(NodeItem node, Group series) {

		//For each item simply add a sphere
		Transform3D tr = new Transform3D();
		double[] d = node.getAttribute(Keys.centroid).get();
		tr.setTranslation(new Vector3d(d[0],d[1],0));

		TransformGroup tg = new TransformGroup();
		tg.setTransform(tr);		

		Sphere sphere = new Sphere();
		sphere.setCapability(Primitive.ENABLE_GEOMETRY_PICKING);
		sphere.setCapability(Primitive.ENABLE_APPEARANCE_MODIFY);
		sphere.setUserData(node.getAttribute(WritableDataItem.idKey));

		tg.addChild(sphere);
		series.addChild(tg);

		return sphere;
	}





	public GeometryArray createMesh(List<float[]> mesh) {
		
		//FIXME change to lines

		Point3f[] points = new Point3f[mesh.size()];
		for(int i = 0; i< points.length; i++){
			float[] f = mesh.get(i);
			points[i] = new Point3f(f[0],f[1],0);
		}

		//Create the geometry from the list of Point3f
		GeometryInfo triangleArray = new GeometryInfo(GeometryInfo.TRIANGLE_ARRAY);
		//triangleArray.setCoordinates(array);//should work!
		triangleArray.setCoordinates(points);
		new NormalGenerator().generateNormals(triangleArray);

		return triangleArray.getIndexedGeometryArray();		
	}







}
