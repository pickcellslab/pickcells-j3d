package org.pickcellslab.pickcells.j3d.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import java.util.Objects;

import javax.media.j3d.Appearance;
import javax.media.j3d.Material;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;
import javax.vecmath.Color3f;

import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.foundationj.ui.renderers.DimensionRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class AppearanceSelector extends JDialog {

	private static Logger log = LoggerFactory.getLogger(AppearanceSelector.class);

	private Appearance currentApp = null;
	private JComboBox<String> comboBox;
	private JSlider transSlider;
	private JSlider shineSlider;
	private JButton SpecBtn;
	private final JButton DifBtn;
	private final JButton emBtn;
	private final JButton ambBtn;

	private JComboBox<String>  dataBox;


	private final JCheckBox anchorBox;

	private final AppearanceManager mgr;

	private final JComboBox<Dimension<?,?>> hmDimBox;
	private final JComboBox<Icon> hmLutBox;
	private final GroupPanel hmPanel;
	private final JCheckBox hmEnableBox;

	private final DataAccess access;



	AppearanceSelector(AppearanceManager mgr, UITheme theme, DataAccess access){


		this.mgr = mgr;
		this.access = access;

		JLabel lblAmbient = new JLabel("Ambient");
		lblAmbient.setToolTipText("the ambient RGB color reflected off the surface of the material");

		ambBtn = new JButton(new ColorIcon());
		ambBtn.addActionListener(l->{
			if(chooseColor(ambBtn)){
				if(currentApp!=null){
					Material mat = currentApp.getMaterial();
					if(null!=mat){
						Color c = ((ColorIcon) ambBtn.getIcon()).getColor();
						mat.setAmbientColor((float)c.getRed()/255f, (float)c.getGreen()/255f, (float)c.getBlue()/255f);					
					}
				}
			}
		});

		JLabel lblEmissive = new JLabel("Emissive");
		lblEmissive.setToolTipText("the RGB color of the light the material emits");

		emBtn = new JButton(new ColorIcon());
		emBtn.addActionListener(l->{
			if(chooseColor(emBtn)){
				if(currentApp!=null){
					currentApp.setTexture(null);
					Material mat = currentApp.getMaterial();
					if(null!=mat){
						Color c = ((ColorIcon) emBtn.getIcon()).getColor();
						mat.setEmissiveColor((float)c.getRed()/255f, (float)c.getGreen()/255f, (float)c.getBlue()/255f);						
					}
				}
			}
		});

		JLabel lblDiffuse = new JLabel("Diffuse");
		lblDiffuse.setToolTipText("he RGB color of the material when illuminated");

		DifBtn = new JButton(new ColorIcon());
		DifBtn.addActionListener(l->{
			if(chooseColor(DifBtn)){
				if(currentApp!=null){
					Material mat = currentApp.getMaterial();
					if(null!=mat){
						Color c = ((ColorIcon) DifBtn.getIcon()).getColor();
						mat.setDiffuseColor((float)c.getRed()/255f, (float)c.getGreen()/255f, (float)c.getBlue()/255f);					
					}
				}
			}
		});

		JLabel lblSpecular = new JLabel("Specular");
		lblSpecular.setToolTipText("the RGB specular color of the material (highlights)");

		SpecBtn = new JButton(new ColorIcon());
		SpecBtn.addActionListener(l->{
			if(chooseColor(SpecBtn)){
				if(currentApp!=null){
					Material mat = currentApp.getMaterial();
					if(null!=mat){
						Color c = ((ColorIcon) SpecBtn.getIcon()).getColor();
						mat.setSpecularColor((float)c.getRed()/255f, (float)c.getGreen()/255f, (float)c.getBlue()/255f);		
						log.info("Specular = "+c.toString());
					}
				}
			}
		});



		JLabel lblShininess = new JLabel("Shininess");
		lblShininess.setToolTipText("the material's shininess");

		shineSlider = new JSlider();
		shineSlider.setMinimum(0);
		shineSlider.setMaximum(128);
		shineSlider.addChangeListener(l->{
			if(!shineSlider.getValueIsAdjusting())             
				if(currentApp!=null){
					Material mat = currentApp.getMaterial();
					if(null!=mat){
						mat.setShininess(shineSlider.getValue());	
						log.info("Shininess = "+shineSlider.getValue());
					}
				}
		});

		JLabel lblTransparency = new JLabel("Transparency");
		lblTransparency.setToolTipText("");

		transSlider = new JSlider();
		transSlider.setMinimum(0);
		transSlider.setMaximum(100);
		transSlider.addChangeListener(l->{
			if(!transSlider.getValueIsAdjusting()) 
				if(currentApp!=null){
					TransparencyAttributes mat = currentApp.getTransparencyAttributes();
					if(null!=mat){
						mat.setTransparency((float)transSlider.getValue()/100f);		
						log.info("Tansparency = "+transSlider.getValue());
					}
				}
		});

		JLabel lblDisplay = new JLabel("Display");
		lblDisplay.setToolTipText("");

		String[] types = {"Point","Line","Fill"};
		comboBox = new JComboBox<>(types);
		comboBox.addItemListener(l->{
			if(currentApp!=null){
				PolygonAttributes att = currentApp.getPolygonAttributes();
				if(null!=att)
					att.setPolygonMode(comboBox.getSelectedIndex());
				log.info("Mode = "+comboBox.getSelectedItem());
			}		
		});


		anchorBox = new JCheckBox("Show Anchors");
		anchorBox.setEnabled(false);
		anchorBox.setToolTipText("Some links may be displayed as an arrow between objects\nor alternatively as an arrow between anchor locations defined by this link\n(between meshes for example)");
		anchorBox.addActionListener(l ->{
			//MetaSearch s = (MetaSearch)dataBox.getSelectedItem();
			//s.setAttribute(SceneView.showAnchors,anchorBox.isSelected());
			//TODO view.toggleAnchors(s);
		});



		// HeatMap Mode
		//---------------------------------------------------------------------------------------------------------
		hmEnableBox = new JCheckBox("Enable HeatMap ");

		JLabel dimLbl = new JLabel("HeatMap Dimension : ");
		hmDimBox = new JComboBox<>();
		hmDimBox.setRenderer(new DimensionRenderer(theme));

		GroupPanel dimPanel = new GroupPanel(10, dimLbl, hmDimBox);



		JLabel lutLbl = new JLabel("HeatMap Lut : ");
		hmLutBox = new JComboBox<>(mgr.getHeatMapIcons());
		//hmLutBox.setRenderer(new TextureRenderer());

		GroupPanel lutPanel = new GroupPanel(10, lutLbl, hmLutBox);

		JButton vBtn = new JButton("Validate");
		vBtn.addActionListener(l->{

			String series = (String) dataBox.getSelectedItem();

			System.out.println("Validating Series : "+series);

			if(hmEnableBox.isSelected()){
				mgr.setHeatMapLut(series, mgr.getHeatMapLuts()[hmLutBox.getSelectedIndex()]);
				mgr.setHeatMapDimension(series, (Dimension) hmDimBox.getSelectedItem(), access);
			}else{
				System.out.println("Removing HeatMap from : "+series);
				mgr.setHeatMapLut(series, null);
			}


		});


		hmPanel = new GroupPanel(10, false, hmEnableBox, dimPanel, lutPanel, vBtn );
		hmPanel.setBorder(BorderFactory.createTitledBorder("HeatMap Mode"));



		// Dataset Choice
		//---------------------------------------------------------------------------------------------------------






		JLabel lblDataset = new JLabel("Dataset");
		lblDataset.setToolTipText("Select the dataset for which the appearance will be changed");

		dataBox = new JComboBox<>();
		mgr.registeredSeries().forEach(s->dataBox.addItem(s));


		dataBox.addItemListener(l->{
			initControllers();
			setVisibility();
		});

		String query = Objects.toString(dataBox.getSelectedItem());
		currentApp = mgr.getAppearance(query);
		initControllers();
		setVisibility();



		// Ok / Cancel
		//---------------------------------------------------------------------------------------------------------



		JButton btnNewButton = new JButton("Cancel");
		btnNewButton.addActionListener(l->{
			for(String s : mgr.registeredSeries()){
				//TODO view.setSeriesVisible(s, true);
			}
			this.dispose();
		});
		//TODO restore original attributes


		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			for(String s : mgr.registeredSeries()){
				//TODO view.setSeriesVisible(s, true);
			}
			this.dispose();
		});






		//Layout
		//---------------------------------------------------------------------------------

		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);


		JPanel panel = new JPanel(new BorderLayout());
		setContentPane(panel);


		GroupPanel topGP = new GroupPanel(10, lblDataset, dataBox, anchorBox);
		panel.add(topGP,BorderLayout.NORTH);


		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Colors", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Surface Attributes", TitledBorder.LEADING, TitledBorder.TOP, null, null));




		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblDisplay, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblTransparency, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblShininess))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
								.addComponent(comboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(shineSlider, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
								.addComponent(transSlider, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE))
						.addGap(12))
				);
		gl_panel_2.setVerticalGroup(
				gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_2.createSequentialGroup()
										.addGap(26)
										.addComponent(lblShininess))
								.addGroup(gl_panel_2.createSequentialGroup()
										.addContainerGap()
										.addComponent(shineSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(transSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_2.createSequentialGroup()
										.addGap(14)
										.addComponent(lblTransparency)))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblDisplay))
						.addContainerGap(32, Short.MAX_VALUE))
				);
		panel_2.setLayout(gl_panel_2);


		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(lblAmbient, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(lblDiffuse, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(lblEmissive)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addComponent(lblSpecular, GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(emBtn, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
								.addComponent(ambBtn, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
								.addComponent(DifBtn, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
								.addComponent(SpecBtn, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel_1.setVerticalGroup(
				gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(ambBtn)
								.addComponent(lblAmbient, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(emBtn, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addGap(11)
										.addComponent(lblEmissive)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(DifBtn, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addGap(11)
										.addComponent(lblDiffuse)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(SpecBtn, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
										.addGap(11)
										.addComponent(lblSpecular)))
						.addContainerGap(36, Short.MAX_VALUE))
				);
		panel_1.setLayout(gl_panel_1);





		GroupPanel centerPanel = new GroupPanel(10, panel_1, panel_2, hmPanel);
		panel.add(centerPanel, BorderLayout.CENTER);



		JPanel okCancelPanel = new JPanel();
		panel.add(okCancelPanel, BorderLayout.SOUTH);


		GroupLayout gl_okCancelPanel = new GroupLayout(okCancelPanel);
		gl_okCancelPanel.setHorizontalGroup(
				gl_okCancelPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_okCancelPanel.createSequentialGroup()
						.addContainerGap(260, Short.MAX_VALUE)
						.addComponent(btnOk)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnNewButton)
						.addContainerGap())
				);
		gl_okCancelPanel.setVerticalGroup(
				gl_okCancelPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_okCancelPanel.createSequentialGroup()
						.addGroup(gl_okCancelPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnNewButton)
								.addComponent(btnOk))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		okCancelPanel.setLayout(gl_okCancelPanel);

		pack();

	}


	private void setVisibility() {
		/*
		currentApp = view.getSeriesAppearance((MetaSearch) dataBox.getSelectedItem());
		if(currentApp!=null){
			for(MetaSearch search : model.loadedSeries())
				if((MetaSearch) dataBox.getSelectedItem()!= search)
					view.setSeriesVisible(search, false);
			view.setSeriesVisible((MetaSearch) dataBox.getSelectedItem(), true);
		} TODO
		 */
	}




	private void initControllers(){

		//Current series
		String series = Objects.toString(dataBox.getSelectedItem());

		currentApp = mgr.getAppearance(series);
		if(currentApp!=null){//Set all the colors for Jbuttons
			Material mat = currentApp.getMaterial();
			if(mat==null){
				mat = new Material();
				mat.setCapability(Material.ALLOW_COMPONENT_READ);
				mat.setCapability(Material.ALLOW_COMPONENT_WRITE);
				currentApp.setMaterial(mat);
			}

			Color3f ambient = new Color3f();
			mat.getAmbientColor(ambient);
			ambient.scale(255f);
			((ColorIcon)ambBtn.getIcon()).setColor(new Color((int)ambient.x,(int)ambient.y,(int)ambient.z));

			Color3f diffuse = new Color3f();
			mat.getDiffuseColor(diffuse);
			diffuse.scale(255f);
			((ColorIcon)DifBtn.getIcon()).setColor(new Color((int)diffuse.x,(int)diffuse.y,(int)diffuse.z));

			Color3f emmisive = new Color3f();
			mat.getEmissiveColor(emmisive);
			emmisive.scale(255f);
			((ColorIcon)emBtn.getIcon()).setColor(new Color((int)emmisive.x,(int)emmisive.y,(int)emmisive.z));

			Color3f specular = new Color3f();
			mat.getSpecularColor(specular);
			specular.scale(255f);
			((ColorIcon)SpecBtn.getIcon()).setColor(new Color((int)specular.x,(int)specular.y,(int)specular.z));

			shineSlider.setValue((int)mat.getShininess());




			TransparencyAttributes ta = currentApp.getTransparencyAttributes();
			if(null!=ta)
				transSlider.setValue((int)(ta.getTransparency()*100f));
			PolygonAttributes att = currentApp.getPolygonAttributes();
			if(null!=att)
				comboBox.setSelectedIndex(att.getPolygonMode());				
		}


		
		
		
		
		// HeatMap Controllers
		//--------------------------------------------------------------------------------------------
		
		
		
		this.hmDimBox.removeAllItems();
		List<Dimension> dims = mgr.availableHeatMapDimensions(series, access);
		boolean enabled = !dims.isEmpty();

		hmPanel.setEnabled(enabled);
		for(int i = 0; i<hmPanel.getComponentCount(); i++){
			hmPanel.getComponent(i).setEnabled(enabled);
		}
		if(!enabled)
			return;
		
		
		Dimension<?,?> dim = mgr.getCurrentHeatMapDimension(series);
		if(dim!=null){
			for(int i = 0; i<hmDimBox.getItemCount(); i++)
				if(((Dimension<?,?>) hmDimBox.getItemAt(i)).name().equals(dim.name())){
					hmDimBox.setSelectedIndex(i);
					break;
				}
			System.out.println("Current Dim is "+dim);
		}

		int index = mgr.getCurrentTextureIndex(series);
		if(index == -1)
			hmEnableBox.setSelected(false);
		
		else{
			hmEnableBox.setSelected(true);
			hmLutBox.setSelectedIndex(index);
		}
				
		mgr.availableHeatMapDimensions(series, access).forEach(d->hmDimBox.addItem(d));

	}


	
	
	


	private boolean chooseColor(JButton btn){		
		ColorIcon icon = (ColorIcon) btn.getIcon();
		Color newColor = JColorChooser.showDialog(null, "Choose Color", icon.getColor());		
		if(newColor!=null && newColor!=icon.getColor()){			
			icon.setColor(newColor);
			return true;
		}
		return false;
	}




}
