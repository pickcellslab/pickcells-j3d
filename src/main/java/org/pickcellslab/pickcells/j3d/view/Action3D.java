package org.pickcellslab.pickcells.j3d.view;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.Group;
import javax.media.j3d.Shape3D;

import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.config.QueryInfo;
import org.pickcellslab.foundationj.mapping.data.DataRegistry;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Boundary;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.SegmentationManagementException;
import org.pickcellslab.pickcells.api.imgdb.view.SegmentationManager;
import org.pickcellslab.pickcells.j3d.scenegraph.Arrow3f;
import org.pickcellslab.pickcells.j3d.view.AppearanceManager.Scenary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.j3d.utils.geometry.Sphere;

public class Action3D implements Action<Path<NodeItem,Link>,BranchGroup> {

	private static final Logger log = LoggerFactory.getLogger(Action3D.class);
	
	private final ExecutorService service = Executors.newWorkStealingPool();

	private final DataRegistry registry;
	private final SegmentationManager segs;
	private final AppearanceManager apps;

	private final Map<Object,Mechanism3D> coords = new HashMap<>();

	private final Scene3DFactory factory;
	
	private final QueryInfo qi;

	private final File home;


	public Action3D(DataRegistry registry, Scene3DFactory factory, SegmentationManager manager, AppearanceManager apps, QueryInfo qi, File home) {
		this.registry = registry;
		this.factory = factory;
		this.segs = manager;
		this.apps = apps;
		this.qi = qi;
		this.home = home;
	}

	@Override
	public ActionMechanism<Path<NodeItem, Link>> createCoordinate(Object key) {
		Mechanism3D m = new Mechanism3D(Objects.toString(key));
		coords.put(key,m);
		return m;
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public BranchGroup createOutput(Object key) throws IllegalArgumentException {
		BranchGroup bg = new BranchGroup();
		coords.get(key).groups.forEach((k,g)-> bg.addChild(g));
		bg.setUserData(key);
		//TODO capabilities


		log.info("Objects created for "+Objects.toString(key));
		coords.get(key).groups.forEach((k,g)->{
			log.info("With "+k.toString()+" having "+g.numChildren()+" childs");
		});


		return bg;
	}

	@Override
	public String description() {
		return "Populate the 3D Scene";
	}





	private class Mechanism3D implements ActionMechanism<Path<NodeItem,Link>>{

		private final List<Future<?>> pending = new ArrayList<>();
		final Map<String, BranchGroup> groups = new HashMap<>();
		final Map<String, MetaQueryable> pointers = new HashMap<>();
		final Set<WritableDataItem> processed = new HashSet<>(); 
		final String prefix;

		public Mechanism3D(String prefix) {
			this.prefix = prefix+" - ";
			//System.out.println("New Mechanism3D for "+prefix);
		}

		@Override
		public void performAction(Path<NodeItem, Link> path) throws DataAccessException {


			//Get the last node
			if(!qi.get(QueryInfo.INCLUDED).isEmpty()){


			//	System.out.println("QInfo not empty");
				
				NodeItem node = path.last();
				if(processed.add(node)){

					try{

						String clazzType = node.typeId();
						String type = node.declaredType();
												
						Class<? extends NodeItem> clazz = (Class<? extends NodeItem>) registry.classOf(clazzType);
						MetaQueryable mq = pointers.get(type);												
						if(mq == null){
							mq = qi.getIncludedForClass(type);
							pointers.put(type, mq);
							
					//		System.out.println("MQ found -> "+mq);
						}
						final MetaQueryable mc = mq;

						if(ImageDot.class.isAssignableFrom(clazz)){					
							Sphere sphere = factory.createDot(node, getNodeGroup(type));
							sphere.setAppearance(getAppearance(type, Scenary.Location));
							sphere.setUserData(node.getAttribute(DataItem.idKey).get());
							apps.addHeatMappable(prefix+type, 
									new HeatMappable<>((GeometryArray) sphere.getShape().getGeometry(),  mc, node.getAttribute(WritableDataItem.idKey).get()));
							
						}
						else{

							final int id = node.getAttribute(DataItem.idKey).get();
							
							//System.out.println("Node ID "+id);
							
							if(clazz == Boundary.class){
								
								final List<float[]> mesh = Boundary.mesh(home, node);
								
								//System.out.println("Boundary Mesh size "+mesh.size());
								
								Future<?> f = service.submit(()->{
									GeometryArray geo = factory.createMesh(mesh);
									Shape3D s = new Shape3D();
									s.setGeometry(geo);								
									s.setAppearance(getAppearance(type, Scenary.Bounds));
									s.setUserData(id); // for pickcing
									getNodeGroup(type).addChild(s);
									apps.addHeatMappable(prefix+type, new HeatMappable<>(geo,  mc, id));
									
									//System.out.println("Boundary Mesh created ");
								});
								pending.add(f);
								
								
								
							}
							else{

								final long[] bbMin = node.getAttribute(Keys.bbMin).get();
								final long[] bbMax = node.getAttribute(Keys.bbMax).get();
								final float label = node.getAttribute(SegmentedObject.labelKey).get();
								
								Future<?> f = service.submit(()->{
									try{
										
										GeometryArray geo = factory.createMesh(getMesh(type, bbMin, bbMax, label));
										Shape3D s = new Shape3D();
										s.setGeometry(geo);								
										s.setAppearance(getAppearance(type, Scenary.Bounds));
										s.setUserData(id);
										getNodeGroup(type).addChild(s);										
										apps.addHeatMappable(prefix+type, new HeatMappable<>(geo, mc, id));
										
									} catch (Exception e) {
										log.warn("An error occured while rendering an object from the database", e);
									}
								});
								pending.add(f);
							}
						}

					}catch(Exception e){
						log.warn("An object encountered in the path cannot be rendered in 3D", e);
					}

				}


			}				




			//Get the last link
			Link l = path.lastEdge();
			if(l != null ){//&& processed.add(l))
				
				//System.out.println("link");
				Arrow3f s = factory.createLink(l);	
				//System.out.println("link returned");
				if(s!=null){
					

				//	System.out.println("link non null");
					s.setUserData(l.getAttribute(DataItem.idKey).get());
					s.setAppearance(getAppearance(l.declaredType(), Scenary.Vector));
					getGroup(l.declaredType(), qi.get(QueryInfo.TARGET) ).addChild(s); 
				}
			}



		}



		private List<float[]> getMesh(String type, long[] bbMin, long[] bbMax, float label) throws SegmentationManagementException {

			long min = bbMax[0] - bbMin[0];
			min = Math.min(bbMax[1] - bbMin[1], min);
			min = Math.min(bbMax[2] - bbMin[2], min);

			int res = (int) Math.min(min, 4);
			res = res > 0 ? res : 1; 

	//		System.out.println("Getting mesh for  -> "+type);

			return segs.getFor(type).getMesh(
					bbMin,
					bbMax,
					label, 0, //TODO time lapse support
					res,
					false, true); 
		}



		private Appearance getAppearance(String s, Scenary sc) {
			return apps.getOrCreateAppearance(prefix+s, sc);
		}


		
		private synchronized Group getNodeGroup(String type){
			BranchGroup bg = groups.get(prefix+type);
			if(bg == null){
				bg = new BranchGroup();
				bg.setUserData(pointers.get(type));
				//TODO set capabilities
				groups.put(prefix+type, bg);
			}
			return bg;
		}


		private synchronized Group getGroup(String s, MetaQueryable mq){
			BranchGroup bg = groups.get(prefix+s);
			if(bg == null){
				bg = new BranchGroup();
				bg.setUserData(mq);
				//TODO set capabilities
				groups.put(prefix+s, bg);
			}
			return bg;
		}



		public void lastAction(){
			try {
				for(Future<?> f : pending){

					f.get();

				}

			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			pending.clear();
		}


	}






















}
