package org.pickcellslab.pickcells.j3d.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseListener;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Light;
import javax.media.j3d.PointLight;
import javax.media.j3d.Switch;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.DataView;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.foundationj.services.theme.UITheme;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPicker;
import org.pickcellslab.pickcells.api.imgdb.view.ImageContentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.picking.PickCanvas;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

public class View3D  implements DataView<ImageContentModel,Path<NodeItem,Link>>{






	private final static Logger log = LoggerFactory.getLogger(View3D.class);


	//SceneGraph fields

	private final Canvas3D canvas3D;	

	private final BranchGroup root;

	private final SimpleUniverse simpleU;

	
	
	//Scene controls
	private final OrbitBehavior orbit;
	private final PickCanvas pickCanvas;
	private final MouseListener[] pickers;
	
	

	// Series Appearances and visibility

	private final AppearanceManager appMgr;

	private final Switch querySwitch;
	//private BitSet queryBitSet = new BitSet(1);

	
	private final TransformGroup masterTG;
	private final TransformGroup lightTG;
	
	

	//Data Management Fields

	private ImageContentModel model;
	
	private final NotificationFactory notif;


	private final Scene3DFactory factory;



	public View3D(ImageContentModel model, Scene3DFactory factory, NotificationFactory notif, UITheme theme) {

		this.factory = factory;
		this.notif = notif;
		
		this.model = model;
		model.addDataEvtListener(this);
		
		this.appMgr = new AppearanceManager(theme);
		
		// Creating a simple universe
		canvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
		canvas3D.setPreferredSize(new Dimension(250, 250));

		simpleU = new SimpleUniverse(canvas3D);
		ViewingPlatform viewingPlatform = simpleU.getViewingPlatform();
		viewingPlatform.setNominalViewingTransform();
		viewingPlatform.getViewPlatform().setActivationRadius(5000);



		// Set the view position back far enough so that we can see things
		TransformGroup viewTransform = viewingPlatform.getViewPlatformTransform();
		Transform3D t3d = new Transform3D();
		// Note: Now the large value works
		t3d.lookAt(new Point3d(0,0,50), new Point3d(0,0,0), new Vector3d(0,1,0));
		t3d.invert();
		viewTransform.setTransform(t3d);

		// Set back clip distance so things don't disappear 
		simpleU.getViewer().getView().setBackClipDistance(1000);

		simpleU.getViewer().getView().setDepthBufferFreezeTransparent(false);





		// Creating root of the scene Graph
		// Including mouse interactions capabilities and some lightings
		root = new BranchGroup();
		root.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		root.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
		root.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		root.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);


		//double[] center = image.dimensions(false);

		orbit = new OrbitBehavior(canvas3D);
		orbit.setSchedulingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 5000));
		orbit.setZoomFactor(20);
		orbit.setTransXFactor(20);
		orbit.setTransYFactor(20);
		orbit.setReverseRotate(true);
		orbit.setReverseZoom(true);
		orbit.setReverseTranslate(true);

		simpleU.getViewingPlatform().setViewPlatformBehavior(orbit);


		masterTG = new TransformGroup();
		masterTG.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		masterTG.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
		masterTG.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		masterTG.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);



		lightTG = new TransformGroup();

		Light directionalLight = new DirectionalLight(new Color3f(1, 1, 1), new Vector3f(0, -1, -1));

		Light ambientLight = new AmbientLight(new Color3f(0.5f, 0.5f, 0.5f));

		ambientLight.addScope(root);

		Light pointLight = new PointLight(new Color3f(0.5f, 0.5f, 0.5f), new Point3f(-1f, 1f, 2f),
				new Point3f(0.5f, 0, -0.5f));

		BoundingSphere bounds = new BoundingSphere(new Point3d(0, 0, 0), 1000);

		directionalLight.setInfluencingBounds(bounds);
		ambientLight.setInfluencingBounds(bounds);
		pointLight.setInfluencingBounds(bounds);

		// lightTG.addChild(directionalLight);
		lightTG.addChild(ambientLight);
		lightTG.addChild(pointLight);



		querySwitch = new Switch(Switch.CHILD_ALL);
		querySwitch.setCapability(Switch.ALLOW_CHILDREN_EXTEND);
		querySwitch.setCapability(Switch.ALLOW_CHILDREN_WRITE);
		querySwitch.setCapability(Switch.ALLOW_SWITCH_READ);
		querySwitch.setCapability(Switch.ALLOW_SWITCH_WRITE);


		masterTG.addChild(querySwitch);

		root.addChild(masterTG);
		root.addChild(lightTG);


		//Picking handling
		pickCanvas = new PickCanvas(canvas3D, root);
		pickCanvas.setMode(PickCanvas.BOUNDS);

		canvas3D.addMouseListener(new Recenter3D(pickCanvas, orbit));
		
		pickers = new MouseListener[]{new SinglePicker3D(pickCanvas, orbit), new MultiPicker3D(pickCanvas, orbit)};
		
		
		
		//TODO listener for recentring --> canvas3D.addMouseListener(this); 


		simpleU.addBranchGraph(root);

		// masterBranchGroup = new BranchGroup();
		// masterBranchGroup.setCapability(BranchGroup.ALLOW_DETACH);

		// masterTG.addChild(masterBranchGroup);

	}

	
	
	public void setPickingControl(MouseListener ml) {
		canvas3D.removeMouseListener(pickers[0]);
		canvas3D.removeMouseListener(pickers[1]);
		if(ml!=null)
			canvas3D.addMouseListener(ml);
	}




	MouseListener[] pickers() {
		return pickers;
	}
	
	public SingleDataPicker singlePicker() {
		return (SingleDataPicker) pickers[0];
	}

	



	@Override
	public void dataSetChanged(DataSet<Path<NodeItem, Link>> oldDataSet, DataSet<Path<NodeItem, Link>> newDataSet) {
		

		Thread t = new Thread(()->{

			try {

				root.detach();
				root.removeAllChildren();
				root.addChild(masterTG);
				root.addChild(lightTG);
				

				for(int q = 0; q<model.getDataSet().numQueries(); q++){

					
					ResultAccess<String, BranchGroup> l = model.getDataSet().loadQuery(q, 
							new Action3D(
									model.dataRegistry(),
									factory,
									model.getSegmentationManager(),
									appMgr,
									model.getDataSet().queryInfo(q), model.containingFolder()));

					for (int s = 0; s<l.size(); s++){
						root.addChild(l.getResult(s));
						log.info("View 3D -> num children "+ l.getResult(s).numChildren());
						log.info("View 3D -> root child added"+ l.getKey(s));
					}

				}

				Transform3D transform = new Transform3D(); 
				Vector3f position = new Vector3f();

				BoundingSphere b = (BoundingSphere) root.getBounds();
				
				Point3d center = new Point3d();
				b.getCenter(center);
				center.set(center.x,center.y,center.z+500);
				
				transform.setTranslation(new Vector3f(center));
				
				//root.getLocalToVworld(transform); 
				transform.get(position);

				simpleU.getViewingPlatform().getViewPlatformTransform().setTransform(transform);

				simpleU.addBranchGraph(root);

			} catch (DataAccessException e) {
				log.error("DataAccess Error",e);
			}


		});


		notif.waitAnimation(t, "Loading Scene, Please wait...");
		


	}




	@Override
	public void setModel(ImageContentModel model) {
		this.model = model;
	}

	@Override
	public ImageContentModel getModel() {
		return model;
	}




	public Component canvas() {
		return canvas3D;
	}



	public AppearanceManager getAppearanceManager() {
		return appMgr;
	}








}
