package org.pickcellslab.pickcells.j3d.view;

import java.util.List;

import javax.media.j3d.GeometryArray;
import javax.media.j3d.Group;

import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.pickcells.j3d.scenegraph.Arrow3f;

import com.sun.j3d.utils.geometry.Sphere;

public interface Scene3DFactory  {

	public Arrow3f createLink(Link l);

	public Arrow3f createAnchoredLink(Link l);

	/**
	 * Creates a sphere places it at the appropriate location and adds it to the provided Group
	 * @param node
	 * @param series
	 * @return
	 */
	public Sphere createDot(NodeItem node, Group series);

	public GeometryArray createMesh(List<float[]> mesh) ;

}
